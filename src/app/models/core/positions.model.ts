export interface IAuditTrail {
    comments: string,
    createdBy: string,
    createdDate: string,
    id: number,
    updatedBy: string,
    updatedOn: string
}

export interface IPositions {
    account: string,
    auditTrail: IAuditTrail,
    buCode: string,
    clientHiringManager: string,
    description: string,
    experienceLevel: string,
    fgId: string,
    location: string,
    postionId: string,
    priority: string,
    recruiter: string, // From service response the values are coming as whole data. 
    resourceManager: string // From service response the values are coming as whole data. 
    techHead: string, // From service response the values are coming as whole data. 
    title: string,
    skills: Array<string>,
    department: string,
    status: string
}