export interface IUser{
    username: string,
    password: string
}

export interface ISystemUser {
    id: number,
    name: string
}
