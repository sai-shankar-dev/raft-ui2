export interface ICandidate {
    id: number,
    candidateName: string,
    candidateEmail: string,
    experience: number,
    joinDate: string,
    link: string,
    location: string,
    offerDate: string,
    skill: Array<string>,
    clientInteractionStatus: string,
    clientOnboardingStatus: string
}

export interface ICreateCandidate {
    candidateName: string,
    candidateEmail: string,
    skill: Array<string>,
    location: string,
    link: string,
    joinDate: string,
    offerDate: string,
    experiencePrior: number
}

/* 
    {
  "candidateName": "string",
  "candidateEmail": "string",
  "skill": [
    "string"
  ],
  "location": "string",
  "link": "string",
  "clientInteractionStatus": "Interview Selected",
  "clientOnboardingStatus": "Started",
  "joinDate": "2021-11-09",
  "offerDate": "string",
  "experiencePrior": 0
}
*/