import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';
import { IUser } from 'src/app/models/core/user.model';
import { BackendService } from 'src/app/services/backend.service';
import { DataSourceService } from 'src/app/services/data-source.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    email: string = "";
    password: string = "";
    loginForm: any;
    constructor( private router: Router, private _dataSourceService: DataSourceService, private _backEndService: BackendService) {
        this.loginForm = new FormGroup({
            username: new FormControl(''),
            password: new FormControl('')
        });
    }

    ngOnInit(): void {
        this._dataSourceService.updateDataSource('route', 'login');

    }
    validate(): void {
        console.log(this.loginForm.value);
        // Implement Service here
        // const user: IUser = {
        //     username: 'AviBaraswal',
        //     password: 'abc123'
        // }
        if (this.loginForm.value) {
            if (this.loginForm.value.username === '' || this.loginForm.value.password === '') {
                alert('Please enter Username and Password !!');
                return;
            }
        }
        this._backEndService.login(this.loginForm.value).subscribe((response: any) => {
            console.log(response);
            if (response && response.token) {
                localStorage.setItem('apiKey', response.token);
                this.router.navigate(['positions']);
            }
        })
    }

}
