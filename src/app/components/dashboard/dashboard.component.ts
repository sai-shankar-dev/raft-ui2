import { Component, OnInit } from '@angular/core';
import { DataSourceService } from 'src/app/services/data-source.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

    constructor(private _dataSourceService: DataSourceService) { }

    ngOnInit(): void {
        this._dataSourceService.getDataSource('route').subscribe((value: string) => {
            if (value !== 'dashboard') {
                this._dataSourceService.updateDataSource('route', 'dashboard');
            }
        })
    }

}
