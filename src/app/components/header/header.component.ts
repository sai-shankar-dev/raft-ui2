import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataSourceService } from 'src/app/services/data-source.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    isHeader: boolean = true;
    selectedMenu: string = '';
    constructor(private _dataSourceService: DataSourceService, private router: Router) { }

    ngOnInit(): void {
        this._dataSourceService.getDataSource('route').subscribe((value: string) => {
            console.log(value);
            this.selectedMenu = value;
            if (value === 'login') {
                this.isHeader = false;
            } else {
                this.isHeader = true;
            }
        })
    }
    onMenuChange(menu: string): void {
        // console.log(menu);
        this.router.navigate([menu]);
        this._dataSourceService.updateDataSource('route', menu);
    }
    getSelectedClass(menu: string): string {
        if (menu === 'candidates') {
            if (this.selectedMenu === 'candidates') {
                return 'selected';
            }
        }
        if (menu === 'positions') {
            if (this.selectedMenu === 'positions') {
                return 'selected';
            }
        }
        return '';
    }
}
