import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { IMetadata } from '../../models/core/metadata.model';
import { IPositions } from '../../models/core/positions.model';
import { BackendService } from '../../services/backend.service';
import { DataSourceService } from '../../services/data-source.service';
import { CreatePositionComponent } from './create-position/create-position.component';

export interface IPositionComponent {
    updatePositionResponseData(positionsResponse: any): Array<any>;
    updateRowSelection(position: IPositions): void;
    getSelectedClass(id: string): string
}

@Component({
    selector: 'app-positions',
    templateUrl: './positions.component.html',
    styleUrls: ['./positions.component.scss']
})
export class PositionsComponent implements OnInit {
    total: number = 0;
    metadata: IMetadata = <any>null;
    rowData: Array<any> = [];
    selectedRow: IPositions = <any>null;
    bsModalRef?: BsModalRef;
    constructor(private _dataSourceService: DataSourceService, private _backendService: BackendService, private _modalService: BsModalService) {
    }

    ngOnInit(): void {
        this._dataSourceService.getDataSource('route').subscribe((value: string) => {
            if (value !== 'positions') {
                this._dataSourceService.updateDataSource('route', 'positions');
            }
        })
        this._backendService.getMetadata().subscribe((metadata: IMetadata) => {
            // console.log(metadata);
            this._dataSourceService.updateDataSource('metadata', metadata);
        })
        this._backendService.getPositions().subscribe((positionsResponse: Array<IPositions>) => {
            console.log(positionsResponse);
            const updatedPositionData = this.updatePositionResponseData(positionsResponse);
            this.rowData = updatedPositionData;
            this.total = this.rowData.length;
            if (this.rowData.length > 0) {
                this.selectedRow = this.rowData[0];
            }
        })
        // [this could have removed and from the first response itself we can achieve what we want]
        this._dataSourceService.getDataSource('metadata').subscribe((metadata: IMetadata) => {
            console.log(metadata);
        })
    }
    updatePositionResponseData(positionsResponse: any): Array<any> {
        // [intermediate data processing. WHich should be removed later]
        const updatedResponse: Array<IPositions> = [];
        if (positionsResponse) {
            positionsResponse.forEach((value: any) => {
                const obj: IPositions = value;
                if (value.recruiter && value.recruiter.name) {
                    obj.recruiter = value.recruiter.name;
                }
                if (value.resourceManager && value.resourceManager.name) {
                    obj.resourceManager = value.resourceManager.name;
                }
                if (value.techHead && value.techHead.name) {
                    obj.techHead = value.techHead.name;
                }
                updatedResponse.push(obj);
            })
            return updatedResponse;
        }
        return [];
    }
    updateRowSelection(position: IPositions): void {
        this.selectedRow = position;
    }
    getSelectedClass(id: string): string {
        if (this.selectedRow) {
            if (id === this.selectedRow.postionId) {
                return 'selected';
            }
        }
        return '';
    }
    openAddPosition(): void {
        console.log('Add Position');
        const initialState: ModalOptions = {
            initialState: {
                list: [
                    'Open a modal with component',
                    'Pass your data',
                    'Do something else',
                    '...'
                ],
                title: 'Modal with component'
            }
        };
        this.bsModalRef = this._modalService.show(CreatePositionComponent, initialState);
        this.bsModalRef.content.closeBtnName = 'Close';
    }
}
