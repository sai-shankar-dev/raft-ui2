import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ColDef } from 'ag-grid-community';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ICandidate, ICreateCandidate } from 'src/app/models/core/candidate.model';
import { IMetadata } from 'src/app/models/core/metadata.model';
import { BackendService } from '../../services/backend.service';
import { DataSourceService } from '../../services/data-source.service';

@Component({
    selector: 'app-candidates',
    templateUrl: './candidates.component.html',
    styleUrls: ['./candidates.component.scss']
})
export class CandidatesComponent implements OnInit {
    columnDefs: ColDef[] = [];
    rowData: Array<any> = [];
    total: number = 0;
    modalRef?: BsModalRef;
    candidateForm: any;
    responseMessage: string = "";
    RESPONSE_SUCCESS_MESSAGE: string = "Candidate added successfully !!";
    RESPONSE_FAILURE_MESSAGE: string = "Not able to add the candidate !!"
    constructor(private _dataSourceService: DataSourceService, private _backEndService: BackendService, private _modalService: BsModalService) {
        this.columnDefs = this.getCandidateGridColDef();
        this.candidateForm = new FormGroup({
            candidateName: new FormControl(''),
            candidateEmail: new FormControl(''),
            experiencePrior: new FormControl(''),
            joinDate: new FormControl(''),
            link: new FormControl(''),
            location: new FormControl(''),
            offerDate: new FormControl(''),
            skill: new FormControl('')
        });
    }

    ngOnInit(): void {
        this._dataSourceService.getDataSource('route').subscribe((value: string) => {
            if (value !== 'candidates') {
                this._dataSourceService.updateDataSource('route', 'candidates');
            }
        })
        this._backEndService.getMetadata().subscribe((metaDataResponse: IMetadata) => {
            console.log(metaDataResponse);
        })
        this.getCandidates();
    }
    getCandidates(): void {
        this._backEndService.getCandidates().subscribe((responseData: Array<any>) => {
            console.log(responseData);
            if (responseData) {
                const updatedData: Array<ICandidate> = [];
                responseData.forEach((response: any) => {
                    const obj: ICandidate = {
                        id: response.id,
                        candidateName: response.candidateName,
                        candidateEmail: response.candidateEmail,
                        experience: (response.candidateDetails) ? response.candidateDetails.experience : null,
                        joinDate: response.joinDate,
                        link: response.link,
                        location: response.location,
                        offerDate: response.offerDate,
                        skill: response.skill,
                        clientInteractionStatus: response.clientInteractionStatus,
                        clientOnboardingStatus: response.clientOnboardingStatus
                    }
                    updatedData.push(obj);
                })
                this.rowData = updatedData;
            } else {
                this.rowData = [];
            }
            this.total = this.rowData.length;
        })
    }
    getCandidateGridColDef(): Array<ColDef> {
        let colDefs: Array<ColDef> = [{
            field: 'id',
            headerName: 'ID',
            width: 60,
            sortable: true,
        }, {
            field: 'candidateName',
            headerName: 'Candidate Name',
            width: 200,
            sortable: true,
        }, {
            field: 'candidateEmail',
            headerName: 'Candidate Email',
            width: 180,
            sortable: true,
        }, {
            field: 'experience',
            headerName: 'Experience',
            width: 120,
            sortable: true,
        }, {
            field: 'joinDate',
            headerName: 'Joined Date',
            width: 130,
            sortable: true,
        }, {
            field: 'link',
            headerName: 'Link',
            width: 250,
            sortable: true,
        }, {
            field: 'location',
            headerName: 'Location',
            width: 120,
            sortable: true,
        }, {
            field: 'offerDate',
            headerName: 'Offer Date',
            width: 160,
            sortable: true,
        }, {
            field: 'skill',
            headerName: 'Skills',
            width: 160,
            sortable: true,
        }, {
            field: 'clientInteractionStatus',
            headerName: 'Client Interaction Status',
            width: 200,
            sortable: true,
        }, {
            field: 'clientOnboardingStatus',
            headerName: 'Client Onboarding Status',
            width: 200,
            sortable: true,
        }];
        return colDefs;
    }
    openModal(candidatetemplate: TemplateRef<any>) {
        this.modalRef = this._modalService.show(candidatetemplate);
    }
    createCandidate(template: TemplateRef<any>): void {
        const candidate: ICreateCandidate = {...this.candidateForm.value};
        candidate.skill = this.candidateForm.value.skill.split(',');
        candidate.experiencePrior = Number(this.candidateForm.value.experiencePrior);
        this.modalRef?.hide();

        this._backEndService.createCandidate(candidate).subscribe((createCandidateResponse: any) => {
            console.log(createCandidateResponse);
            if (createCandidateResponse) {
                this.modalRef?.hide();
                console.log(typeof createCandidateResponse);
                this.modalRef = this._modalService.show(template);
                this.responseMessage = this.RESPONSE_SUCCESS_MESSAGE;
            }
        })
    }
    updateCandidateGrid(): void {
        this.modalRef?.hide();
        this.getCandidates();
    }

}
