import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IUser } from '../models/core/user.model';
import { ICreateCandidate } from '../models/core/candidate.model';

export interface IBackendService {
    login(user: IUser): Observable<any>;
    getPositions(): Observable<any>
}

export class API_CONSTANTS {
    static readonly BASE_URL: string = 'https://raft-accolite-digital.tk/';
    static readonly LOGIN: string = 'user/authenticate';
    static readonly POSITIONS: string = 'position/get';
    static readonly METADATA: string = 'position/positionmetadata';
    static readonly CANDIDATES: string = 'candidate/get';
    static readonly CREATE_CANDIDATE: string = 'candidate/savecandidate'
}
@Injectable({
    providedIn: 'root'
})
export class BackendService implements IBackendService {
    constructor(private _httpClient: HttpClient) { }
    login(user: IUser): Observable<any> {
        const httpOptions = {
            headers: new HttpHeaders({ 'Content-Type': 'application/json' })
        };
        return this._httpClient.post(API_CONSTANTS.BASE_URL + API_CONSTANTS.LOGIN, user, httpOptions)
    }
    getRequestHeader(): HttpHeaders {
        return new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('apiKey')
        })
    }
    getPositions(): Observable<any> {
        return this._httpClient.get(API_CONSTANTS.BASE_URL + API_CONSTANTS.POSITIONS, { headers: this.getRequestHeader() })
    }
    getMetadata(): Observable<any> {
        return this._httpClient.get(API_CONSTANTS.BASE_URL + API_CONSTANTS.METADATA, { headers: this.getRequestHeader() })
    }
    getCandidates(): Observable<any> {
        return this._httpClient.get(API_CONSTANTS.BASE_URL + API_CONSTANTS.CANDIDATES, { headers: this.getRequestHeader() })
    }
    createCandidate(candidate: ICreateCandidate): Observable<any> {
        const httpOptions = {
            headers: new HttpHeaders({ 'Content-Type': 'application/json' })
        };
        return this._httpClient.post(API_CONSTANTS.BASE_URL + API_CONSTANTS.CREATE_CANDIDATE, candidate, { headers: this.getRequestHeader() })
    }
}
