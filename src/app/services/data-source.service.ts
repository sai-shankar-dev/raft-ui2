import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

export interface IDataSourceService {
    createDataSource(): void;
    getDataSource(source: string): void;
    updateDataSource(source: string, value: any): void
}

interface IDataSource {
    route: any;
}

class DataSource implements IDataSource {
    route: any = null;
    metadata: any = null
    constructor() {
        this.route = new BehaviorSubject(null);
        this.metadata = new BehaviorSubject(null);
    }
}
@Injectable({
    providedIn: 'root'
})
export class DataSourceService implements IDataSourceService {
    private _dataSource: any;
    constructor() {
    }
    createDataSource(): void {
        this._dataSource = new DataSource();
    }
    getDataSource(source: string): Observable<any> {
        return this._dataSource[source].asObservable();
    }
    updateDataSource(source: string, value: any): void {
        if (this._dataSource && this._dataSource[source]) {
            this._dataSource[source].next(value);
        }
    }
}
