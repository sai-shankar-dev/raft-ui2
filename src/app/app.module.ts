import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { AgGridModule } from 'ag-grid-angular';
import { ModalModule } from 'ngx-bootstrap/modal';
import { StoreModule } from '@ngrx/store';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

// [application components]
import { LoginComponent } from './components/login/login.component';
import { PositionsComponent } from './components/positions/positions.component';
import { CandidatesComponent } from './components/candidates/candidates.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HeaderComponent } from './components/header/header.component';

// [application Services]
import { DataSourceService } from './services/data-source.service';
import { CreatePositionComponent } from './components/positions/create-position/create-position.component';

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        PositionsComponent,
        CandidatesComponent,
        DashboardComponent,
        HeaderComponent,
        CreatePositionComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        ReactiveFormsModule,
        AgGridModule.withComponents([]),
        ModalModule.forRoot(),
        StoreModule.forRoot({})
    ],
    providers: [
        DataSourceService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
