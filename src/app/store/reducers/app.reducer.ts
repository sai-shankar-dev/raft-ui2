import { Action, createReducer, on } from '@ngrx/store';
import { LOGIN } from '../actions/login.action';
import { INITIAL_STATE, IState } from '../state';

const _appReducer = createReducer(INITIAL_STATE,
    // on(LOGIN, (state: IState, {LOGIN}) => ({LOGIN})),
);

export function appReducer(state: IState, action: Action) {
    return _appReducer(state, action);
}