export interface IState {
    SELECTED_ROUTE: string
}

export const INITIAL_STATE: IState = {
    SELECTED_ROUTE: 'login'
}