import { createAction } from '@ngrx/store';

export const LOGIN = createAction('[Login Component] Login');